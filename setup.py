from setuptools import setup

setup(name='vststaskcmd',
      version='0.2.1',
      description='A simple wrapper for using VSTS task commands',
      license='MIT',
      packages=['vststaskcmd'],
      zip_safe=False)